#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_VIEW_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_VIEW_H
#pragma once
#include <iostream>
#include "Tower.h"
#include <vector>
#include "../Square/Square.h"

using std::cout;

class View {
public:
    ~View() {
        delete field;
    }
private:
    friend class Map;
    const std::vector<std::vector<Square*>>* field;

    View(const std::vector<std::vector<Square*>>& field_) { this->field = &field_; }

    void DrawObjects() const;
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_VIEW_H
