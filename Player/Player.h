#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_PLAYER_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_PLAYER_H
#pragma once
#include <iostream>
#include "FordBellmanFactory.cpp"
#include "DijkstraFactory.cpp"
#include "BfsFactory.cpp"
#include <vector>
class Player {
private:
    friend class Builder;
    friend class GoldManBuilder;
    friend class StrikerBuilder;
    friend class DefenderBuilder;
    friend class Game;
    std::string nickName;
    std::vector<Tower*> army;
    int health;
    void setNickName(std::string nickName_);
    void setTower(Tower* tower);;
    void setHealth(int health);
    int bfsStat;
    int fordBellmanStat;
    int dijkstraStat;
public:
    const std::vector<Tower*>& getArmy();;
    const std::string& getName();
    const int& getHealth();

    ~Player();
};

class Builder {
public:
    Player* getRezult() const;
private:
    friend class Maker;
protected:
    virtual ~Builder();;
    virtual void buildName(std::string name_ ) = 0;
    virtual void buildArmy(int firstCount_, int secondCount_, int thirdCount_) = 0;
    virtual void buildHealth(int health_) = 0;
    Player* _rezult;
};

class GoldManBuilder : public Builder {
public:
    GoldManBuilder();
    void buildName(std::string name_) override;

    void buildArmy(int firstCount_, int secondCount_, int thirdCount_) override;

    void buildHealth(int health_) override;
};

class StrikerBuilder : public Builder {
public:
    StrikerBuilder();
    void buildName(std::string name_) override;

    void buildArmy(int firstCount, int secondCount, int thirdCount) override;

    void buildHealth(int health_) override;
};

class DefenderBuilder : public Builder {
public:
    DefenderBuilder();

    void buildName(std::string name_) override;

    void buildArmy(int firstCount, int secondCount, int thirdCount) override;

    void buildHealth(int health_) override;
};

class Maker {
public:
    void setBuilder(Builder* builder_);

    void construct(const std::string& nick_, int firstCount, int secondCount, int thirdCount, int health_);
    Builder* builder;
    ~Maker();
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_PLAYER_H