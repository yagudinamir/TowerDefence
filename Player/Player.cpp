#include "Player.h"
void Player::setNickName(std::string nickName_) {
    this->nickName = nickName_;
}

void Player::setTower(Tower *tower) {
    army.push_back(tower);
}

void Player::setHealth(int health) {
    this->health = health;
}

const std::vector<Tower *> &Player::getArmy() {
    return army;
}

const std::string &Player::getName() {
    return nickName;
}

const int &Player::getHealth() {
    return health;
}

Player::~Player() {
    for (size_t i = 0; i < army.size(); ++i) {
        delete army[i];
    }
}

GoldManBuilder::GoldManBuilder() {
    _rezult = new Player();
}

void GoldManBuilder::buildName(std::string name_) {
    _rezult->setNickName(name_);
}

void GoldManBuilder::buildArmy(int firstCount_, int secondCount_, int thirdCount_) {
    for (int i = 0; i < firstCount_; ++i) {
        BfsFactory* factory = new BfsFactory();
        _rezult->setTower(factory->createTower());
    }
    for (int i = 0; i < secondCount_; ++i) {
        DijkstraFactory* factory = new DijkstraFactory();
        _rezult->setTower(factory->createTower());
    }
    for (int i = 0; i < thirdCount_; ++i) {
        FordBellmanFactory* factory = new FordBellmanFactory();
        _rezult->setTower(factory->createTower());
    }
}

void GoldManBuilder::buildHealth(int health_) {
    _rezult->setHealth(health_);
}

StrikerBuilder::StrikerBuilder() {
    _rezult = new Player();
}

void StrikerBuilder::buildName(std::string name_) {
    _rezult->setNickName(name_);
}

void StrikerBuilder::buildArmy(int firstCount, int secondCount, int thirdCount) {
    for (int i = 0; i < firstCount; ++i) {
        BfsFactory* factory = new BfsFactory();
        _rezult->setTower(factory->createTower());
        _rezult->bfsStat++;
    }
    for (int i = 0; i < secondCount; ++i) {
        DijkstraFactory* factory = new DijkstraFactory();
        _rezult->setTower(factory->createTower());
        _rezult->dijkstraStat++;
    }
    for (int i = 0; i < thirdCount; ++i) {
        FordBellmanFactory* factory = new FordBellmanFactory();
        _rezult->setTower(factory->createTower());
        _rezult->fordBellmanStat++;
    }
}

void StrikerBuilder::buildHealth(int health_) {
    _rezult->setHealth(health_);
}

DefenderBuilder::DefenderBuilder() {
    _rezult = new Player();
}

void DefenderBuilder::buildName(std::string name_) {
    _rezult->setNickName(name_);
}

void DefenderBuilder::buildArmy(int firstCount, int secondCount, int thirdCount) {
    for (int i = 0; i < firstCount; ++i) {
        BfsFactory* factory = new BfsFactory();
        _rezult->setTower(factory->createTower());
        _rezult->bfsStat++;
    }
    for (int i = 0; i < secondCount; ++i) {
        DijkstraFactory* factory = new DijkstraFactory;
        _rezult->setTower(factory->createTower());
        _rezult->dijkstraStat++;
    }
    for (int i = 0; i < thirdCount; ++i) {
        FordBellmanFactory* factory = new FordBellmanFactory();
        _rezult->setTower(factory->createTower());
        _rezult->fordBellmanStat++;
    }
}

void DefenderBuilder::buildHealth(int health_) {
    _rezult->setHealth(health_);
}

Player* Builder::getRezult() const {
    return _rezult;
}

Builder::~Builder() {
    delete _rezult;
}

void Maker::construct(const std::string& nick_, int firstCount, int secondCount, int thirdCount, int health_) {
    builder->buildName(nick_);
    builder->buildArmy(firstCount, secondCount, thirdCount);
    builder->buildHealth(health_);
}

void Maker::setBuilder(Builder *builder_) {
    builder = builder_;
}

Maker::~Maker() {
//    delete builder;
}
