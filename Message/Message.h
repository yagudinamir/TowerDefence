#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_MESSAGE_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_MESSAGE_H
#include <iostream>
#pragma once

class Message{
private:
    friend class Game;
    friend class EmptySquare;
    friend class Square;
    inline static void Print(std::string message_) {
        std::cout << message_;
    }
    ~Message();
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_MESSAGE_H
