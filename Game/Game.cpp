#include "Game.h"
Game* Game::g_instance;

Player* Game::getPlayer() {
    return player;
}

void Game::Start() {
    Message::Print("Please write your name...\n\n");
//    std::cin >> name;
    name = "Max";
    Message::Print("Well, now please choose your detachment...\n\nPrint (1) for Striker\nPrint (2) for Defender\nPrint (3) for Goldman\n...\n");
//       std::cin >> detachment;
    detachment = 1;
    Message::Print("Excellent!!! Now choose nick for your character...\n");
//       std::cin >> nick;
    nick = "SHAKER";
    Message::Print("Well, ");
    Message::Print(name);
    Message::Print(", now please choose your difficulty...\n\nPrint (1) for HardMode\nPrint (2) for Medium\nPrint (3) for Easy\n...\n");
//        std::cin >> difficulty;
    difficulty = 1;
    Message::Print("\n\nDownloading...........................\n\nGame Started.\n #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*\n\n");
    GameInitialize_map();
    BuildPlayer();
        //...Printing Starting Complect of the towers and then asking user to put them on map...//
        //...The first invasion goes further...//
    }

void Game::AddTower(int firstCount, int secondCount, int thirdCount) {
    std::vector<Tower*> tmp = player->getArmy();
    for (int i = 0; i < firstCount; ++i) {
        BfsFactory* factory = new BfsFactory();
        tmp.push_back(factory->createTower());
        player->bfsStat++;
    }
    for (int i = 0; i < secondCount; ++i) {
        DijkstraFactory* factory = new DijkstraFactory();
        tmp.push_back(factory->createTower());
        player->dijkstraStat++;
    }
    for (int i = 0; i < thirdCount; ++i) {
        FordBellmanFactory* factory = new FordBellmanFactory();
        tmp.push_back(factory->createTower());
        player->fordBellmanStat++;
    }
}

void Game::GameInitialize_map() {
    map = Map::instance();
}

void Game::DrawNextInvasion() {
        Map* tmp = map;
        //...MODIFY_TMP...//
        tmp->Draw();
}

void Game::ReadInput() {
    int t1;
    int t2;
    std::cin >> t1 >> t2;
    currentInput.clear();
    currentInput.push_back(t1);
    currentInput.push_back(t2);
}

void Game::WriteOutput() const {}

void Game::Process() {
    Message::Print("$$$$MAP$$$$\n");
    map->Draw();
    Message::Print("Well ,");
    Message::Print(name);
    Message::Print(", now please protect the city from the next *Z0MBIE INVASION* \n");
    DrawNextInvasion();
    Message::Print("Build TOWERS to protect the city...\n");
    ReadInput();
    //...PROCESS...//
    WriteOutput();
}

void Game::BuildPlayer() {
    switch (detachment) {
        case 1 :
        {
            StrikerBuilder strikerBuilder;
            Maker maker;
            maker.setBuilder(&strikerBuilder);
            maker.construct(nick, 1, 2, 2, 200);
            player = strikerBuilder.getRezult();
            Message::Print(player->getName());
            Message::Print("\n");
            break;
        }
        case 2 :
        {
            DefenderBuilder defenderBuilder;
            Maker maker;
            maker.setBuilder(&defenderBuilder);
            maker.construct(nick, 2, 0, 3, 100);
            player = defenderBuilder.getRezult();
            Message::Print(player->getName());
            break;
        }
        case 3:
        {
            GoldManBuilder goldmanBuilder;
            Maker maker;
            maker.setBuilder(&goldmanBuilder);
            maker.construct(nick, 2, 2, 1, 300);
            player = goldmanBuilder.getRezult();
            Message::Print(player->getName());
            break;
        }
    }
}

Game::Game() {}
