#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_GAME_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_GAME_H
#pragma once
#include <iostream>
#include <vector>
#include "Map.h"
#include "../Message/Message.h"
#include "../Player/Player.h"

class Game {
private:

    Map* map;
    static Game* g_instance;
    explicit Game();
    std::vector<int> currentInput;
    std::string name;
    int detachment;
    int difficulty;
    std::string nick;
    Player* player;

public:

    Game(const Game&) = delete;
    Game& operator=(const Game&) = delete;
    ~Game() = delete;

    inline static Game* GameInstance() {
        if (g_instance == NULL) {
            g_instance = new Game();
        }
        return g_instance;
    }

    Player* getPlayer();

    void Start();

    void ReadInput();;

    void WriteOutput() const;

    void Process();

    void BuildPlayer();

    void DrawNextInvasion();

//    template<typename T>
//    std::vector<T*> GetPlayerArmy() {
//        std::vector<T*> tmp;
//        tmp = player->getArmy();
//        return tmp;
//    }

    void AddTower(int firstCount, int secondCount, int thirdCount);

    void GameInitialize_map();

};
#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_GAME_H
