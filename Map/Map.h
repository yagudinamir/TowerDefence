#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_MAP_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_MAP_H
#pragma once
#include <iostream>
#include <vector>
#include "../Square/Square.h"
#include "View.h"


class Map {
private:
//    static std::shared_ptr<Map> m_instance;
//    static std::shared_ptr<Map> m_instance;
    static Map* m_instance;

    inline void init(unsigned int width, unsigned int length);

    Map();


    unsigned int width;
    unsigned int length;
    std::vector<std::vector<Square*>> field;

public:
    friend class Game;
    Map(const Map&) = delete;
    Map& operator=(const Map&) = delete;
    ~Map() = delete;

    inline static Map* instance() {
        if (m_instance == NULL) {
//      m_instance = std::shared_ptr<Map>(new Map);
//            m_instance = std::make_shared<Map>(Map());
            m_instance = new Map();
        }
        return m_instance;
    };

    void Draw();
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_MAP_H
