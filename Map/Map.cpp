#include "Map.h"

Map* Map::m_instance;

void Map::Draw() {
    View* view = new View(this->field);
    view->DrawObjects();
}

inline void Map::init(unsigned int width, unsigned int length) {
    Map::field.assign(width, std::vector<Square*>(length));
}

Map::Map() : width(50), length(50) {
    this->init(width, length);
}