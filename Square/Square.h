#pragma once
#include <iostream>
#include "../Message/Message.h"

class Square {
protected:
    friend class View;
    friend class DijkstraTower;
    friend class BfsTower;
    friend class FordBellmanTower;
    friend class Tower;
    unsigned int x;
    unsigned int y;
    std::string ch;
    inline void Print() const {
        Message::Print(ch);
    }
    virtual ~Square() = default;
};