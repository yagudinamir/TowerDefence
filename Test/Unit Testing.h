#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_UNIT_TESTING_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_UNIT_TESTING_H
#include "../Game/Game.h"
#include <iostream>

TEST(Building, PlayerHealth) {
    Game* game = Game::GameInstance();
    game->Start();
    int h = game->getPlayer()->getHealth();
    EXPECT_EQ(h, 200);
}

TEST(Building, PlayerNick) {
    Game* game = Game::GameInstance();
    game->Start();
    std::string s = game->getPlayer()->getName();
    EXPECT_EQ(s, "SHAKER");
}

TEST(Factory, BF) {
    BfsFactory* factory = new BfsFactory();
    Tower* tower = factory->createTower();
    tower->setDamage(100);
    tower->setHealth(100);
    tower->setRange(100);
    tower->setPrice(100);
    int d = tower->getDamage();
    int h = tower->getHealth();
    int p = tower->getPrice();
    int r = tower->getRange();
    long long ans = d*1000000 + 10000*h + 100*p + r;
    EXPECT_EQ(101010100, ans);
}

TEST(Factory, DF) {
    DijkstraFactory* factory = new DijkstraFactory();
    Tower* tower = factory->createTower();
    tower->setDamage(100);
    tower->setHealth(100);
    tower->setRange(100);
    tower->setPrice(100);
    int d = tower->getDamage();
    int h = tower->getHealth();
    int p = tower->getPrice();
    int r = tower->getRange();
    long long ans = d*1000000 + 10000*h + 100*p + r;
    EXPECT_EQ(101010100, ans);
}

TEST(Factory, FBF) {
    FordBellmanFactory* factory = new FordBellmanFactory();
    Tower* tower = factory->createTower();
    tower->setDamage(100);
    tower->setHealth(100);
    tower->setRange(100);
    tower->setPrice(100);
    int d = tower->getDamage();
    int h = tower->getHealth();
    int p = tower->getPrice();
    int r = tower->getRange();
    long long ans = d*1000000 + 10000*h + 100*p + r;
    EXPECT_EQ(101010100, ans);
}

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_UNIT_TESTING_H
