#include "Tower.h"
void Tower::SetXYCoordinates(int x_, int y_) {
    x = x_;
    y = y_;
}
int Tower::getRange () const {
    return range;
}
int Tower::getHealth() const {
    return health;
};
int Tower::getPrice() const {
    return price;
};
int Tower::getDamage() const {
    return damage;
};

void Tower::setRange(int range_) {
    range = range_;
};
void Tower::setHealth(int health_) {
    health = health_;
};
void Tower::setPrice(int price_) {
    price = price_;
};
void Tower::setDamage(int damage_) {
    damage = damage_;
};

void Tower::Statistics() const {
    std::cout << "Damage: " << getDamage() << std::endl;
    std::cout << "Health: " << getHealth() << std::endl;
    std::cout << "Price: " << getPrice() << std::endl;
    std::cout << "Range: " << getDamage() << std::endl;
}

bool Tower::DecreaseHealth(int damage_) {
    if (damage_ >= health) {
        health = 0;
        return false;
    } else {
        health -= damage_;
    }
    return true;
}