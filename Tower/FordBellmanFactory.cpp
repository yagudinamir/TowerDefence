#include "TowerFactory.h"
#include "FordBellmanTower.h"
class FordBellmanFactory : public TowerFactory {
public:
    Tower* createTower() override { return new FordBellmanTower(); };
};