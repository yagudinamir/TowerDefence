#include "Tower.h"
#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_TOWERFACTORY_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_TOWERFACTORY_H
#pragma once
class TowerFactory {
private:
    virtual Tower* createTower() = 0;
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_TOWERFACTORY_H
