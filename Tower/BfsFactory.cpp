#include "TowerFactory.h"
#include "BfsTower.h"
class BfsFactory : public TowerFactory {
public:
    Tower* createTower() override { return new BfsTower(); };
};
