#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_HEADER_FOR_CONSTANTS_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_HEADER_FOR_CONSTANTS_H
/* FordBellmanTower */
const int FBrange = 5;
const int FBdamage = 15;
const int FBHealth = 100;
const int FBPrice = 10;

/* DijkstraTower */

const int Drange = 8;
const int DDamage = 10;
const int DHealth = 90;
const int DPrice = 8;

/* BfsTower */

const int Brange = 6;
const int Bdamage = 12;
const int BHealth = 120;
const int BPrice = 12;

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_HEADER_FOR_CONSTANTS_H
