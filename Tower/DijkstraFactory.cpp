#include "TowerFactory.h"
#include "DijkstraTower.h"
class DijkstraFactory : public TowerFactory {
public:
    Tower* createTower() override { return new DijkstraTower(); };
};
