#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_DIJKSTRATOWER_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_DIJKSTRATOWER_H
#include "Tower.h"
#pragma once
class DijkstraTower : public Tower {
private:
    void Shoot(std::vector<std::vector<Square*>>& field_) override;
    void Delete() override;
    void Modify() override;

    int range;
    int damage;
    int health;
    int price;

    int x;
    int y;

    std::string ch = "D";
public:
    explicit DijkstraTower();
    virtual ~DijkstraTower() override = default;
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_DIJKSTRATOWER_H
