#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_TOWER_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_TOWER_H
# pragma once
#include <vector>
#include "../Message/Message.h"
#include "../Square/Square.h"
#include "Header for constants.h"
class Tower : public Square {
protected:
    std::string ch;

    int x;
    int y;

    int range;
    int damage;
    int health;
    int price;

    std::vector<bool> used;
    virtual void Shoot(std::vector<std::vector<Square*>>& field_) = 0;

    virtual void Delete() = 0;
    virtual void Modify() = 0;

    void SetXYCoordinates(int x_, int y_);

    bool DecreaseHealth(int damage_);
public:
    int getRange() const;
    int getHealth() const;
    int getPrice() const;
    int getDamage() const;

    void Statistics() const;

    void setRange(int range_);
    void setHealth(int health_);
    void setPrice(int price_);
    void setDamage(int damage_);

    explicit Tower() {}
    virtual ~Tower() {
        used.clear();
    };
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_TOWER_H
