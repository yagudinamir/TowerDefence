#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_BFSTOWER_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_BFSTOWER_H
#include "Tower.h"
#pragma once

class BfsTower : public Tower {
private:
    void Shoot(std::vector<std::vector<Square*>>& field_) override;
    void Delete() override;
    void Modify() override;

    int range;
    int damage;
    int health;
    int price;

    int x;
    int y;

    std::string ch = "B";
public:
    explicit BfsTower();
    virtual ~BfsTower() override = default;
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_BFSTOWER_H