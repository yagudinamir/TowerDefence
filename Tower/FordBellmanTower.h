#ifndef PROGRAMMING_TECHNOLOGIES_PROJECT_FORDBELLMANTOWER_H
#define PROGRAMMING_TECHNOLOGIES_PROJECT_FORDBELLMANTOWER_H
#include "Tower.h"
#pragma once
class FordBellmanTower : public Tower {
private:
    void Shoot(std::vector<std::vector<Square*>>& field_) override;
    void Delete() override;
    void Modify() override;

    int range;
    int damage;
    int health;
    int price;

    int x;
    int y;

    std::string ch = "F";
public:
    explicit FordBellmanTower();
    virtual ~FordBellmanTower() override = default;
};

#endif //PROGRAMMING_TECHNOLOGIES_PROJECT_FORDBELLMANTOWER_H
